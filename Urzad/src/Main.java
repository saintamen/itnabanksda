import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		WaitingRoom room1 = new WaitingRoom();
		WaitingRoom room2 = new WaitingRoom();
		WaitingRoom room3 = new WaitingRoom();

		Client c1 = new Client("a", "1");
		Client c2 = new Client("ab", "12");
		Client c3 = new Client("av", "13");
		Client c4 = new Client("ac", "14");
		Client c5 = new Client("ad", "5");
		Client c6 = new Client("ae", "16");
		Client c7 = new Client("aw", "178");
		Client c8 = new Client("aa", "18");

		room1.addClient(c1);
		room1.addClient(c2);
		room1.addClient(c3);

		room2.addClient(c4);
		room2.addClient(c5);
		room2.addClient(c6);

		room3.addClient(c7);
		room3.addClient(c8);
		
		OfficeEmployee pracownik = new OfficeEmployee("Janek");
		pracownik.registerClient(room1.getFirstFromQueue()); // handleClient to gerFirstFromQueue
		pracownik.registerClient(room2.getFirstFromQueue());
		pracownik.registerClient(room3.getFirstFromQueue());
		pracownik.registerClient(room3.getFirstFromQueue());
		pracownik.registerClient(room3.getFirstFromQueue());
		
		Scanner in = new Scanner(System.in);
		String typ = in.next();
		
		ClientConcern typConcern = ClientConcern.valueOf(typ);
	}
}
