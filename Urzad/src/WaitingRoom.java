import java.util.LinkedList;
import java.util.Queue;

public class WaitingRoom {

	private Queue<Client> kolejka = new LinkedList<Client>();
	private static int licznik = 0;
	
	public WaitingRoom() {
	}
	
	public void addClient(Client nowyKlient){
		kolejka.add(nowyKlient);
		
		System.out.println("Rozmiar kolejki: "+ kolejka.size());
		System.out.println("Jesteś klientem numer: " + (++licznik));
	}
	
	public Client getFirstFromQueue(){
		if(!kolejka.isEmpty()){
			return kolejka.poll(); // co powoduje pobranie i usunięcie elementu z przodu kolejki
		}
		return null;
	}
	
}
