import java.util.ArrayList;
import java.util.List;

public class User {
	private String login;
	private String password;
	private Sex sex;
	
	public User(String login, String password, Sex param1) {
		super();
		this.login = login;
		this.password = password;
		this.sex = param1;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getPassword() {
		return password;
	}
	
	@Override
	public String toString() {
		return "User={login="+login+", password="+password+"}";
	}
	
	public static void main(String[] args) {
		List<User> usersList = new ArrayList<User>();
		usersList.add(new User("Login1", "Password1", Sex.Female));
		usersList.add(new User("Login2", "Password", Sex.Female));
		usersList.add(new User("Login3", "Passwor", null));
		usersList.add(new User("Login4", "Passw", Sex.Male));
		usersList.add(null);
		
		System.out.println(usersList);
//		for(User user : usersList){
//			System.out.println(user);
//		}
	}
	
}
