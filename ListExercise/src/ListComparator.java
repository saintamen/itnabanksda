import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListComparator {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		int[] table = new int[10];

		Random r = new Random();

		for (int i = 0; i < 123; i++) {
			list.add(r.nextInt(100));
		}

		for (int i = 0; i < 10; i++) {
			table[i] = r.nextInt(100);
		}

		// countIdentical(list, table);

		int a = 100;
		int b = 0;
		zeroDivision(a, b);
	}

	private static void zeroDivision(int a, int b) {
		try {
			double value = a / b;
			System.out.println(value);
		} catch (ArithmeticException ae) {
			System.out.println("Dzielenie przez zero." );
		}
	}

	private static void countIdentical(List<Integer> list, int[] table) {
		try {
			int counter = 0;
			for (int i = 0; i < list.size(); i++) {
				if (table[i] == list.get(i)) {
					counter++;
				}
			}
			System.out.println("There are " + counter + " identical numbers.");
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			System.out.println("Tablice są różnych rozmiarów.");
			return;
		}

	}
}
