import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BasicListExercises {
	private static final int ELEMENTS_COUNT = 200;

	public static void main(String[] args) {
		List<Double> list = new ArrayList<Double>();
		Random rand = new Random();

		for (int i = 0; i < ELEMENTS_COUNT; i++) {
			list.add(rand.nextDouble());
		}

		double sum = countSum(list);
		double avg = countAvg(list);
		double prod = countProduct(list);

		System.out.println("Sum: " + sum + ", avg: " + avg + ", prod:" + prod);

	}

	public static double countSum(List<Double> list) {
		double sum = 0;
		for (Double element : list) {
			sum += element;
		}
		return sum;
	}

	public static double countAvg(List<Double> list) {
		return countSum(list) / list.size();
	}

	public static double countProduct(List<Double> list) {
		double prod = 1;
		for (Double element : list) {
			prod *= element;
		}
		return prod;
	}
}
