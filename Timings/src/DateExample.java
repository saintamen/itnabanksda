import java.text.SimpleDateFormat;
import java.util.Date;

public class DateExample {

	public static void main(String[] args) {
		Date d = new Date();
		System.out.println(d);
		
		Date d2 = new Date(System.currentTimeMillis() - 60000);
		System.out.println(d2);
		
		Long timestamp = System.currentTimeMillis();
		System.out.println(timestamp);
		timestamp = System.currentTimeMillis();
		System.out.println(timestamp);
		timestamp = System.currentTimeMillis();
		System.out.println(timestamp);
		timestamp = System.currentTimeMillis();
		System.out.println(timestamp);
		timestamp = System.currentTimeMillis();
		System.out.println(timestamp);
		timestamp = System.currentTimeMillis();
		System.out.println(timestamp);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		System.out.println(sdf.format(d2));
	}
}
