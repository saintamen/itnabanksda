import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Java8Dates {
	public static void main(String[] args) {
		LocalDate lDate = LocalDate.now();
		System.out.println(lDate);
		
		LocalDate lDate2 = LocalDate.of(2013, 2, 3);
		System.out.println(lDate2);
		
		LocalDate lDate3 = LocalDate.ofEpochDay(System.currentTimeMillis()/86400000);
		System.out.println(lDate3);
		
		LocalTime lTime = LocalTime.now();
		System.out.println(lTime);
		
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);

		LocalDateTime ldt2 = LocalDateTime.of(lDate2, lTime);
		System.out.println(ldt2);
		
		DateTimeFormatter sdformatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		System.out.println(ldt.format(sdformatter));
		
		System.out.println(ldt2.isBefore(ldt));
	}
}
