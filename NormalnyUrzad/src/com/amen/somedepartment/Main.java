package com.amen.somedepartment;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Department d = new Department();
		// d.addOffice(new Office()); // 0
		// d.addOffice(new Office()); // 1
		// d.addOffice(new Office()); // 2
		// d.addOffice(new Office()); // 3
		// d.addOffice(new Office()); // 4
		// d.addOffice(new Office()); // 5

		Scanner sc = new Scanner(System.in);
		String liniaWejscia = null;

		while (sc.hasNextLine()) {
			liniaWejscia = sc.nextLine();

			parse(liniaWejscia, d);
		}
	}

	// jakies sobie zdanie
	// string[0] string[1] string[2]
	private static void parse(String liniaWejscia, Department department) {
		String[] slowa = liniaWejscia.split(" ");

		switch (slowa[0]) {
		case "dodaj_biuro":
			department.addOffice(new Office());
			break;
		case "pobierz_biuro":
			String numerBiura = slowa[1];
			try {
				int numerBiuraJakoInt = Integer.parseInt(numerBiura);
				System.out.println(department.getOffice(numerBiuraJakoInt));
			} catch (NumberFormatException nfe) {
				System.out.println("Zly format komendy.");
			}

			break;
		case "dodaj_klienta":
			String pesel = slowa[1];
			String hisCase = slowa[2];
			Client clientToHandle = new Client(pesel, ClientCase.valueOf(hisCase));

			int idPokoju = Integer.parseInt(slowa[3]);
			department.getOffice(idPokoju).handle(clientToHandle);
			break;
		default:
			System.out.println("Zla komenda");
		}
	}

}
