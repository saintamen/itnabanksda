package com.amen.providers;

import java.math.BigInteger;
import java.util.Random;

public class RandomDataProvider implements DataProvider{
	
	@Override
	public int nextInt(String name) {
		System.out.println("Podaj " + name + ": ");
		Random randomObject = new Random();
		
		return randomObject.nextInt(100);
	}
	
	@Override
	public String nextString(String name) {
		System.out.println("Podaj " + name + ": ");
		Random random = new Random();
		
		return new BigInteger(130, random).toString(32);
	}
}
