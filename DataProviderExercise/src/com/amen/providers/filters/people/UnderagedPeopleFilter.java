package com.amen.providers.filters.people;

import com.amen.providers.ConsoleDataProvider;
import com.amen.providers.DataProvider;
import com.amen.providers.filters.DataFilter;

public class UnderagedPeopleFilter implements DataFilter {
	@Override
	public boolean isOk(int number) {
		if(number > 18){
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println("Aplikacja Filtr Even:");
		DataProvider provider = new ConsoleDataProvider();
		DataFilter filtr = new UnderagedPeopleFilter();

		// pobieramy z providera rozmiar tablicy
		int rozmiarTablicy = provider.nextInt("rozmiar tablicy");
		Person[] tablica = new Person[rozmiarTablicy]; // alokacja tablicy
													// (stworzenie)
		for(int i=0; i< rozmiarTablicy; i++){
			String imie = provider.nextString("imie " + i); // pobieram z wejścia imie
			int wiek = provider.nextInt("wiek " + i);	// pobieram z wejścia wiek
			
			tablica[i] = new Person(imie, wiek); // tworze osobę wykorzystując podane imie i wiek.
		}

		for(int i=0; i< rozmiarTablicy; i++){
			if(filtr.isOk(tablica[i].getAge())){ // weryfikuje wiek (filtrem)
				System.out.println(tablica[i].getName()); // wypisuje imie osoby pelnoletniej
			}
		}
		

	}
}
