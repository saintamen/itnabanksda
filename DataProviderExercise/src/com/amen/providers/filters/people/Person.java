package com.amen.providers.filters.people;

public class Person {
	private String name;
	private int age;
	
	public Person(String name, int ag) {
		this.name = name;
		this.age = ag;
	}
	
	public int getAge() {
		return age;
	}
	
	public String getName() {
		return name;
	}
}
