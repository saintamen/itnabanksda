package com.amen.providers.filters;

import com.amen.providers.ConsoleDataProvider;
import com.amen.providers.DataProvider;

public class EvenNumberFilter implements DataFilter{

	@Override
	public boolean isOk(int number) {
		if(number%2 == 0)
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		System.out.println("Aplikacja Filtr Even:");
		DataProvider provider = new ConsoleDataProvider();
		EvenNumberFilter filtr = new EvenNumberFilter();

		// pobieramy z providera rozmiar tablicy
		int rozmiarTablicy = provider.nextInt("rozmiar tablicy");
		int[] tablica = new int[rozmiarTablicy]; // alokacja tablicy (stworzenie)

		for (int i = 0; i < rozmiarTablicy; i++) {
			tablica[i] = provider.nextInt("element o indeksie " + i); // wczytanie elementów tablicy z providera
		}
		
		// iterujemy tablice
		for( int i=0; i<rozmiarTablicy; i++){
			if(filtr.isOk(tablica[i])){ // filtrem sprawdzamy czy liczba jest 'Ok' - czyli czy jest parzysta
				System.out.println(tablica[i]); // jeśli jest parzysta, to ją wypisujemy
			}
		}

	}
}
