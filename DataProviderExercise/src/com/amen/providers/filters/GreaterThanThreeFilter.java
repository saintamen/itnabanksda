package com.amen.providers.filters;

import com.amen.providers.ConsoleDataProvider;
import com.amen.providers.DataProvider;

public class GreaterThanThreeFilter implements DataFilter{

	@Override
	public boolean isOk(int number) {
		zobacz();
		if(number>3)
			return true;
		return false;
	}
	public void zobacz(){
		
	}
	
	public static void main(String[] args) {
		System.out.println("Aplikacja Filtr Greater than 3:");
		DataProvider provider = new ConsoleDataProvider();
		DataFilter filtr = new GreaterThanThreeFilter();
//		DataFilter filtr = new EvenNumberFilter();

		// pobieramy z providera rozmiar tablicy
		int rozmiarTablicy = provider.nextInt("rozmiar tablicy");
		int[] tablica = new int[rozmiarTablicy]; // alokacja tablicy (stworzenie)

		for (int i = 0; i < rozmiarTablicy; i++) {
			tablica[i] = provider.nextInt("element o indeksie " + i); // wczytanie elementów tablicy z providera
		}
		
		
		// iterujemy tablice
		for( int i=0; i<rozmiarTablicy; i++){
			if(filtr.isOk(tablica[i])){ // filtrem sprawdzamy czy liczba jest 'Ok' - czyli czy jest parzysta
				System.out.println(tablica[i]); // jeśli jest parzysta, to ją wypisujemy
			}
		}

	}
}
