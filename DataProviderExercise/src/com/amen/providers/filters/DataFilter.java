package com.amen.providers.filters;

public interface DataFilter {
	boolean isOk(int number);
}
