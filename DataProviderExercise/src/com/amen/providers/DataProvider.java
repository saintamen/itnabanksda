package com.amen.providers;

public interface DataProvider {

	int nextInt(String name);
	String nextString(String name);
}
