package com.amen.providers;

public class Main {

	// public static void main(String[] args) {
	// DataProvider variable = new RandomDataProvider();
	//
	// System.out.println(variable.nextString("imie"));
	// System.out.println(variable.nextString("nazwisko"));
	// System.out.println(variable.nextInt("wiek"));
	//
	// }

	public static void main(String[] args) {
		DataProvider provider = new ConsoleDataProvider();

		int rozmiarTablicy = provider.nextInt("rozmiar tablicy");
		int[] tablica = new int[rozmiarTablicy];

		int sum = 0;
		for (int i = 0; i < rozmiarTablicy; i++) {
			tablica[i] = provider.nextInt("element o indeksie " + i);
			sum += tablica[i];
		}

		System.out.println("Suma: " + sum);
	}

}
