package com.amen.records;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		Database database = new Database();
		// utworzenie bazy

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
 
			if(line.equals("quit")){
				break;
			}else if(line.equals("refresh")){
				database.refresh();
				continue;
			}else if(line.equals("print")){
				database.print();
				continue;
			}
			String[] splits = line.split(" ");
			parseSplits(splits, database);
		}
	}

	private static void parseSplits(String[] splits, Database database) {
		// weryfikacja ilości parametrów
		if (splits.length < 4) {
			System.err.println("Niewłaściwa ilość parametrów.");
			return;
		}

		
		//pierwszy element parsowania / analizy to konwersja string na datę w odpowiednim formacie
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH:mm");
		LocalDateTime ldt = null;
		try {
			ldt = LocalDateTime.parse(splits[0], formatter);
		} catch (DateTimeParseException e) {
			System.err.println("Zły format daty - proszę o podanie daty w formacie: yyyy-MM-dd-HH:mm");
			return;
		}
		long czas_waznosci = 0;
		try {
			czas_waznosci = Long.parseLong(splits[1]);
		} catch (NumberFormatException nfe) {
			System.err.println("Niepoprawna liczba podana jako parametr 2: " + nfe.getMessage());
			return;
		}
		
		int id = 0;
		try {
			id = Integer.parseInt(splits[2]);
		} catch (NumberFormatException nfe) {
			System.err.println("Niepoprawna liczba podana jako parametr 3: " + nfe.getMessage());
			return;
		}

		// bad..
		// zły string.. niedobry string 
		// suboptymalny - przepisuje bufor
//		String str = "";
//		for(int i =3 ; i< splits.length ; i++){
//			str += splits[i] + " ";
//		}
//		str = str.substring(0, str.length()-1);
//		System.out.println("Linia:" + str);

		StringBuilder str = new StringBuilder();
		for(int i =3 ; i< splits.length ; i++){
			str.append(splits[i]).append(" ");
		}
		String finalString = str.substring(0, str.length()-1);
		System.out.println("Linia:" + finalString);
		
		Record r = new Record(ldt, czas_waznosci, id, finalString);
		database.addRecord(r);
	}
}
