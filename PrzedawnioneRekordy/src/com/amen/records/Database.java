package com.amen.records;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Database {
	Map<Integer, Record> dane = new HashMap<Integer, Record>();

	public Database() {
		// TODO Auto-generated constructor stub
	}

	public void addRecord(Record r) {
		dane.put(r.getIdentyfikator(), r);
	}

	public void refresh() {
		// bad
		// for(Record r : dane.values()){
		// LocalDateTime kopiaPrzesunieta =
		// r.getCzas_dodania().plusMinutes(r.getCzas_waznosci());
		// if(LocalDateTime.now().isAfter(kopiaPrzesunieta)){
		// dane.remove(r.getIdentyfikator());
		// }
		// }
		Iterator<Record> it = dane.values().iterator();
		while(it.hasNext()) {
			Record r = it.next();
			LocalDateTime kopiaPrzesunieta = r.getCzas_dodania().plusMinutes(r.getCzas_waznosci());
			if (LocalDateTime.now().isAfter(kopiaPrzesunieta)) {
				it.remove();
			}
		}
	}
	
	public void print(){
		for(Record r : dane.values()){
			System.out.println(r);
		}
	}
}
