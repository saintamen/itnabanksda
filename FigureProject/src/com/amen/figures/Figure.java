package com.amen.figures;

public interface Figure {
	double countArea();
	double countCircumference();
}
