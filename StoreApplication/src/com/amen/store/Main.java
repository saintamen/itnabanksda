package com.amen.store;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		
		Store s = new Store();
		s.add(new Product(Category.ALCOHOL, "piwo", 2));
		s.add(new Product(Category.ELECTRONICS, "zegarek", 29));
		s.add(new Product(Category.ELECTRONICS, "komputer", 28));
		s.add(new Product(Category.CLOTHES, "bluzka", 25));
		s.add(new Product(Category.FOOD, "kebab", 23));
		s.add(new Product(Category.CLOTHES, "czapka", 23));
		
		// użycie metody dodawania z konsoli
		addProductFromConsole(s);
		
		s.showProduct(Category.ELECTRONICS);
		
	}
	
	public static void addProductFromConsole(Store sklepDoKtoregoDodajemy){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Wpisz Kategorię, nazwę produktu oraz jego cenę:");
		Category kategoria = Category.valueOf(sc.next());
		// Category pozwala na konwersję stringa na jeden z enumów ale tylko
		// pod warunkiem że poprawnie wpiszemy kategorie na wejście (wartości tylko: ALCOHOL, ELECTRONICS, CLOTHES, FOOD)
		String nazwa = sc.next();
		int cena = sc.nextInt();
		
		Product nowyProdukt = new Product(kategoria, nazwa, cena);
		sklepDoKtoregoDodajemy.add(nowyProdukt);
	}

}
