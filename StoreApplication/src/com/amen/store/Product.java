package com.amen.store;

public class Product {

	private Category category;
	private String name;
	private int cena;
	
	public Product(Category category, String name, int cena) {
		this.category = category;
		this.name = name;
		this.cena = cena;
	}

	public Category getCategory() {
		return category;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + "," + cena;
	}

}
