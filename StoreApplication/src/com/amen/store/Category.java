package com.amen.store;

public enum Category {
	
	FOOD, 
	CLOTHES, 
	ELECTRONICS, 
	ALCOHOL

}
